# CodeDeploy EC2

_Install and run CodeDeploy agent on an AWS EC2 instance_

## Ubuntu

```sh
sudo apt install -y wget
wget https://raw.githubusercontent.com/chiefmikey/codedeploy-ec2/main/codedeploy-ec2-ubuntu.sh
sudo chmod +x ./codedeploy-ec2-ubuntu.sh
sudo ./codedeploy-ec2-ubuntu.sh
```

## Linux

```sh
sudo yum install -y wget
wget https://raw.githubusercontent.com/chiefmikey/codedeploy-ec2/main/codedeploy-ec2-linux.sh
sudo chmod +x ./codedeploy-ec2-linux.sh
sudo ./codedeploy-ec2-linux.sh
```
